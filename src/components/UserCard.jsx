import React, {Component} from "react";
import {
    Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button} from 'reactstrap';

import CardModal from "./CardModal";
import TagFetcher from "./show_data/TagFetcher";

class UserCard extends Component {
    constructor(props) {
        super(props);
        this.state = {cardBgColor: "white"}
    }

    handleCardBgColor = async (colorValue) => {
        this.setState({cardBgColor: colorValue});
        console.log("jalan")
    }


    render() {
        return (
            <div>

                <Card style={{marginBottom:"4%"}}>

                    <CardBody style={{backgroundColor:this.state.cardBgColor}}>
                        <h4>{this.props.username}</h4>
                        <TagFetcher username={this.props.username} tagColor={this.handleCardBgColor}/>
                    </CardBody>
                </Card>
            </div>
        );
    }

}


export default UserCard;
