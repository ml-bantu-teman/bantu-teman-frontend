/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const CardModal = (props) => {


    const {
        buttonLabel,
        className
    } = props;

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);



    return (
        <div>
            <Button color="secondary" onClick={toggle}>{buttonLabel}See Tweets</Button>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>Tweets by {props.username}</ModalHeader>
                <ModalBody>
                    <ul>
                        {props.tweetsList.map((value, index) => {
                            return <li key={index}>{value}</li>
                        })}
                    </ul>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Close</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default CardModal;
