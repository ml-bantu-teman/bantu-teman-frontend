import React, {Component} from 'react';
import ShowData from "./show_data/ShowData"
import {Button, Form, FormGroup, Label, Input, Fade, Container, Row, Col} from 'reactstrap';
import UserCard from "./UserCard";
import Card from "reactstrap/es/Card";

class FormBantu extends Component {
    constructor(props) {
        super(props);
        this.state = {username: '', flag: false, scrapper: 'bantutemanv2', coba: "aslinih"};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUrlScrapper = this.handleUrlScrapper.bind(this);
    }

    handleChange(event) {
        this.setState({username: event.target.value, flag: false, scrapper: this.state.scrapper});
    }

    handleUrlScrapper(event) {
        console.log(event.target.value)
        this.setState({username: this.state.username, flag: false, scrapper: event.target.value});
    }

    handleSubmit(event) {
        console.log('A name was submitted: ' + this.state.username);
        console.log('scrapper: ' + this.state.scrapper);
        event.preventDefault();
        this.setState({username: this.state.username, flag: true, scrapper: this.state.scrapper});

    }

    render() {
        return (
            <Container>
                <Row className="justify-content-center">
                    <Form onSubmit={this.handleSubmit} style={{textAlign: "left", width: "22em"}}>
                        <FormGroup>
                            <Label for="username" style={{fontSize: "0.8em"}}>Twitter Username :</Label>
                            <Input type="text" placeholder="Input Twitter Username" value={this.state.username}
                                   onChange={this.handleChange}/>
                        </FormGroup>
                        {this.props.showSetting && <FormGroup>
                            <Label for="scrapper">Select</Label>
                            <Input type="select" value={this.state.scrapper} onChange={this.handleUrlScrapper}>
                                <option value="bantutemanv2">Api Bantu Teman V2</option>
                                <option value="bantutemanv1">Api Bantu Teman V1</option>
                                <option value="local">Localhost:8000</option>
                            </Input>
                        </FormGroup>}
                        <div style={{textAlign: "end"}}>
                            <Button type="submit" color="primary">Submit</Button>
                        </div>
                    </Form>
                </Row>
                <br/><br/>
                <Fade in={this.state.flag}>
                    <div>
                        <br/>
                        {this.state.flag &&
                        <ShowData username={this.state.username} scrapper={this.state.scrapper}/>}
                        {/*<Col lg="4"><UserCard username={this.state.username} scrapper={this.state.scrapper}/></Col>*/}
                    </div>


                </Fade>


            </Container>

        );
    }
}

export default FormBantu;
