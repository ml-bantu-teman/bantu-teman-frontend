import React, {Component} from 'react';
import TagFetcher from './TagFetcher'
import UserCard from "../UserCard";
import {Col} from "reactstrap";

class ShowData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list_following: []
        }
    }

    async componentDidMount() {
        const urlFetch = await fetch("https://api-bantuteman.herokuapp.com/twitterdataminer/getfollowing/" + this.props.username);
        const json_data = await urlFetch.json();
        console.log(json_data.followings);
        this.setState({list_following: json_data.followings})



    }

    render() {

        return (
            <div className="row">
                {
                    this.state.list_following.map((value, index) => {
                        // return <p key={index}>
                        //     <h2>{value} </h2>
                        //     <TagFetcher username={value} scrapper={this.props.scrapper}></TagFetcher></p>
                        // })
                        return <Col key={index} lg="3"><UserCard username={value}
                                                     scrapper={this.state.scrapper}/></Col>
                    })
                }

            </div>
        );
    }
}

export default ShowData;
