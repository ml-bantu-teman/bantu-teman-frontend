import React, {Component} from 'react';
import CardModal from "../CardModal";
import {CardSubtitle} from "reactstrap";

class TagFetcher extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tag: "Loading....", tweets: ["Loading...."], color:"gray"
        }
    }

    async componentDidMount() {
        console.log(this.props.username)
        let url = "https://api-bantutemanv2.herokuapp.com/sentiment-analysis/single/";
        if (this.props.scrapper === "local") {
            url = "http://localhost:8000/sentiment-analysis/single-local/";
        } else if (this.props.scrapper === "bantutemanv1") {
            url = "https://api-bantuteman.herokuapp.com/sentiment-analysis/single/";
        }
        const urlFetch = await fetch(url + this.props.username);
        const json_data = await urlFetch.json();
        console.log(json_data.tag);
        console.log(json_data.tweets);

        if (json_data.tag === "positive") {
            this.setState({tag: json_data.tag, tweets: json_data.tweets, color: "yellowgreen"})
            this.props.tagColor("aqua")
        } else {
            this.setState({tag: json_data.tag, tweets: json_data.tweets, color: "red"})
            this.props.tagColor("coral")
        }

    }

    render() {
        return (
            <div>
                <CardSubtitle style={{marginTop:"5%", marginBottom:"5%", fontWeight:"bold"}}>Tag : <span style={{backgroundColor:this.state.color, paddingTop:"3px", paddingBottom:"5px", paddingLeft:"10px", paddingRight:"10px", color:"white"}} className="rounded-pill">{this.state.tag}</span></CardSubtitle>
                <CardModal username={this.props.username} tweetsList={this.state.tweets}/>
            </div>
        );
    }
}

export default TagFetcher;
