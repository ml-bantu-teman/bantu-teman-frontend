import React, {Component} from 'react';

class Submit extends Component {
    state = {
        count: 0
    };

    handleClick = () => {
        this.setState({count: this.state.count + 1});
    };

    render() {
        return (
            <React.Fragment>
                <form>
                    Username:<br></br>
                    <input type="text" name="username">
                    </input>
                </form>
                <button onClick={this.handleClick}>
                    submit
                </button>
                <p>
                    The Output is here: <br></br>
                </p>
                <div>
                    <span>
                        {this.formatCount()}
                    </span>
                </div>
            </React.Fragment>
        )
    }

    formatCount() {
        const {count} = this.state;
        return count === 0 ? "Zero" : count;
    }
}

export default Submit
