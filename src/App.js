import React, {Component} from 'react';
import './App.css';
import Submit from "./components/Submit"
import FormBantu from "./components/FormBantu"


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSetting: false
        };

        this.showSetting = this.showSetting.bind(this);

    }

    showSetting() {
        console.log("showSetting Clicked")
        this.setState(state => ({
            showSetting: !this.state.showSetting
        }));

    }

    render() {
        return (
            <div className="App">
                <div id="setting-container">
                    <img id="setting" src="https://static.thenounproject.com/png/196271-200.png"
                         onClick={this.showSetting} alt="setting"/>
                </div>
                <div id="content">
                    <h1 id="title">BantuTeman</h1>
                    <FormBantu showSetting={this.state.showSetting}/>

                </div>
            </div>
        );
    }
}

export default App;
