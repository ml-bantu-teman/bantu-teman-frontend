**Made with React**

**Frontend Repository for http://mesinlerning2019.herokuapp.com/**

A Sentiment Analysis app that uses Machine Learning and Naive Bayes Algorithm.

About the app : 
[https://www.youtube.com/watch?v=tgikqWfsiEw](https://www.youtube.com/watch?v=tgikqWfsiEw)

Backend Repository : 
https://gitlab.com/ml-bantu-teman/bantu-teman-backend


Made By:
*  Kevin Raikhan Zain ([https://gitlab.com/kevinraikhan](https://gitlab.com/kevinraikhan))
*  Muhammad Adipashya Yarzuq ([https://gitlab.com/Falkro](https://gitlab.com/Falkro))
*  Muhammad Feril Bagus P. ([https://gitlab.com/feril68](https://gitlab.com/feril68))
*  Stefanus Khrisna A. H. ([https://gitlab.com/khrisnaa3](https://gitlab.com/khrisnaa3))